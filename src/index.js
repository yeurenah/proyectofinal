const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');

/** Init */
const app = express();

app.set('port',process.env.PORT || 3000);
app.set('views',path.join(__dirname,'views'));
app.engine('.hbs',exphbs({
    defaultLayout:'main',
    layoutsDir: path.join(app.get('views'),'layouts'),
    partialsDir: path.join(app.get('views','partials')),
    extname: '.hbs'
}));
app.set('view engine','.hbs');
//Conf

//Middlewares

//Variables globales

//Routes

//archuvos staticos


//servidor
app.listen(app.get('port'),() =>{
console.log('servidor listo');
});